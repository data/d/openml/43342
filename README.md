# OpenML dataset: German-House-Prices

https://www.openml.org/d/43342

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Projects are a great way to learn data science. So I started my own. The numerous housing data sets on Kaggle were the inspiration for this data set. Predicting housing prices is a simple yet insightful regression problem. Understanding data takes time, and the more people analyze it, the faster the secrets can be uncovered. I acquired the data by scraping Immo Scout 24, a marketplace for German real estate.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43342) of an [OpenML dataset](https://www.openml.org/d/43342). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43342/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43342/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43342/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

